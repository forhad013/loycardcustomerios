//
//  MainPageViewController.swift
//  Loycard
///Volumes/Mixed/IOS Project/quizzlar/Quizzler-iOS11/Quizzler/ViewController.swift
//  Created by Forhad on 12/25/17.
//  Copyright © 2017 invertemotech. All rights reserved.
//

import UIKit


import Firebase
import FirebaseGoogleAuthUI
import SCLAlertView
import QRCode
import FirebaseAuth
import FirebaseAuthUI

import FirebaseCore
import FirebaseDatabase
import FirebaseStorage






class QrViewController: UIViewController , FUIAuthDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    let providers: [FUIAuthProvider] = [FUIGoogleAuth()]
    var uid : String = String()
    var currentUser : User? = Auth.auth().currentUser;
    var firstTime = true
    var userId : String = ""
    var ref: DatabaseReference! = Database.database().reference()
    @IBOutlet weak var uidLabel: UILabel!
    @IBOutlet weak var qrImageView: UIImageView!
    
    var hasUser  : Bool = false
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        
        //  CardDetailsViewController.delegate = self
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        ref.removeAllObservers()
        self.firstTime = true
    }
    
    func retrieveMessages(){
        
        
        print("retrive","retrive")
        let messageDB  = ref.child("LoyaltyCards").queryOrdered(byChild: "customerID").queryEqual(toValue: uid)
        
        messageDB.observe(.childChanged, with: { (snapshot) in
            let snapShotValue = snapshot.value as? [String: Any]
           
            if(self.firstTime && self.hasUser){
                self.firstTime = false
            }else if(!self.firstTime && self.hasUser){
                self.showDialog()
                
            }
        }) { (error) in
            print(error)
            if error != nil{
                self.retrieveMessages()
            }
        }
        
    }
    
    
    
    func showDialog(){
        
        let color = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        let appearance = SCLAlertView.SCLAppearance(
            kCircleTopPosition: 2.00, kCircleHeight: 55, kCircleIconHeight: 40, showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("OK"){
            alertView.dismiss(animated: true, completion: nil)
        }
        alertView.showCustom("Congratulation!!", subTitle: "Purchase Count Successfully Updated", color: color, icon: UIImage(named : "ic_success_white.png")!)
        
        //   alertView.showSuccess("Congratulation!", subTitle: "Purchase Count Successfully Updated")
        
    }
    
    
    
    
    func authUI(_ authUI: FUIAuth, didSignInWith user: User?, error: Error?) {
        // handle user and error as necessary
        
        if(error==nil){
            currentUser = user
            userId = (user?.uid)!
            hasUser = true
            print(user)
            self.firstTime = true
            setData(user: user!)
            
           
        }
        
    }
    
    
    func showSuccessDialog(){
        let alertView = SCLAlertView()
        
        alertView.addButton("Done") {
            print("Second button tapped")
            alertView.dismiss(animated: true, completion: nil)
        }
        alertView.showSuccess("Button View", subTitle: "This alert view has buttons")
        self.present(alertView, animated: true, completion: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        print("firstimeViewDidAppear",self.firstTime)
        let user = Auth.auth().currentUser
        
        if let user = user {
            hasUser = true
            setData(user: user);
            
        }else{
            self.firstTime = true
            openSignInView()
            
        }
        
        
    }
    
    func openSignInView(){
        let authUI = FUIAuth.defaultAuthUI()
        authUI?.delegate = self
        authUI?.providers = providers
        self.firstTime = true
        let authViewController = authUI!.authViewController()
        self.present(authViewController, animated: true, completion: nil)
        
    }
    
    
    func setData(user: User){
        
        //    ProgressHUD.show();
        
        uid = user.uid
        let email = user.email
        let photoURL = user.photoURL
        
        
        uidLabel.text = uid
        titleLabel.text = user.displayName
        
        print("firstimeSetData",self.firstTime)
        
        retrieveMessages()
        
        qrImageView.image = {
            var qrCode = QRCode(uid)!
            qrCode.size = self.qrImageView.bounds.size
            qrCode.errorCorrection = .High
            return qrCode.image
        }()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func application(_ app: UIApplication, open url: URL,
                     options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        let sourceApplication = options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String?
        if FUIAuth.defaultAuthUI()?.handleOpen(url, sourceApplication: sourceApplication) ?? false {
            return true
        }
        // other URL handling goes here.
        return false
    }
    
    
    
    
    
    
    
}
