//
//  RewardTableViewController.swift
//  Loycard
//
//  Created by Forhad on 1/2/18.
//  Copyright © 2018 invertemotech. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseAuthUI
import Firebase
import FirebaseCore
import FirebaseDatabase
import FirebaseStorage
import EzImageLoader

class RewardTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UISearchResultsUpdating {
   
 
         let searchController = UISearchController(searchResultsController: nil)
    var position = Int()
    let storage = Storage.storage()
    var rewardArray : [RewardModel] = [RewardModel]()
    var filteredRewardArray : [RewardModel] = [RewardModel]()
    var currentUser : User? = Auth.auth().currentUser
    
    var i : Int = 0
    @IBOutlet weak var tableView: UITableView!
    
    var userId : String = ""
    var ref: DatabaseReference! = Database.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
      
      
        let storageRef = storage.reference()
        
        if currentUser != nil{
            
            userId = (currentUser?.uid)!
        }
        
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        
        retrieveReward()

   
    }
    
    override func viewDidAppear(_ animated: Bool) {
          retrieveReward()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        // If we haven't typed anything into the search bar then do not filter the results
        if searchController.searchBar.text! == "" {
            filteredRewardArray = rewardArray
        } else {
            // Filter the results
            filteredRewardArray = rewardArray.filter { $0.vendorName.lowercased().contains(searchController.searchBar.text!.lowercased()) ||
                $0.rewardDesc.lowercased().contains(searchController.searchBar.text!.lowercased()) }
        }
        
        self.tableView.reloadData()
    }

    override func viewDidDisappear(_ animated: Bool) {
        ref.removeAllObservers()
    }
    
    
    func retrieveReward(){
        
        
        
        let messageDB  = ref.child("LoyaltyRewards").queryOrdered(byChild: "customerID").queryEqual(toValue: userId)
        
        
        
        messageDB.observe(.childAdded, with: { (snapshot) in
            let snapShotValue = snapshot.value as? [String: Any]
            
            
      
           /// print(" main lopp value is \(self.i)" )
          
            
            let rewardModel = RewardModel ()
  
            
            
            let key = snapshot.key
            
            let customerID = snapShotValue?["customerID"]! as! String
            let cardID_customerID = snapShotValue?["cardID_customerID"]! as! String
            let offerID = snapShotValue?["offerID"]! as! String
            let rewardDesc = snapShotValue?["rewardDesc"]! as! String
            let rewardsAvailable = snapShotValue?["rewardsAvailable"]! as! String

            let vendorID = snapShotValue?["vendorID"]! as! String
            
            
            
            rewardModel.customerID = customerID
            rewardModel.key = key
            rewardModel.offerID = offerID
            rewardModel.cardID_customerID = cardID_customerID
            rewardModel.rewardDesc = rewardDesc
            rewardModel.rewardsAvailable = rewardsAvailable
     
            rewardModel.vendorID = vendorID
        
        
            
            self.rewardArray.append(rewardModel)
            
            self.retrieveVendorDetails(vendorID: vendorID, offerID: offerID,i: self.i)
            self.i = self.i+1
            
        }) { (error) in
            print(error)
            if error != nil{
                self.retrieveReward()
            }
        }
        
     
        
    }
    
    
    func retrieveVendorDetails(vendorID :String , offerID: String, i : Int){
        
        
        
        let messageDB  = Database.database().reference().child("Vendors").child(vendorID)
        
        
        messageDB.observe(.value, with: { (snapshot) in
            let snapShotValue = snapshot.value as? [String: Any]
            print(snapShotValue)
            let vendorName = snapShotValue?["businessName"]! as! String
            let businessAddress = snapShotValue?["businessAddress"]! as! String
            self.rewardArray[i].vendorName = vendorName
            self.rewardArray[i].vendorAddress = businessAddress
    
            
            self.filteredRewardArray = self.rewardArray
            self.tableView.reloadData()
            
            
        }) { (error) in
            if(error != nil){
                self.retrieveVendorDetails(vendorID: vendorID, offerID: offerID,i: self.i)
            }
        }
       
     
    }
    
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return filteredRewardArray.count
    }

    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "rewardCell", for: indexPath) as! RewardCellViewController
        
 //
     //   cell.mainView.layer.cornerRadius = 8
   //     cell.clipsToBounds = true
        cell.vendorName.text = filteredRewardArray[indexPath.row].vendorName
        cell.rewardLebel.text = filteredRewardArray[indexPath.row].rewardDesc
 
        
        let offerID = filteredRewardArray[indexPath.row].offerID
        let vendorID = filteredRewardArray[indexPath.row].vendorID
        //        let pathReference = storage.reference(withPath: "Images/\(offerID)/\(vendorID)")
        
        cell.vendorLogo.image = UIImage.init(named: "logo.png")
        
        
        storage.reference().child("Images/\(offerID)/\(vendorID)").downloadURL(completion: { (
            url, error) in
            if(error == nil){
                
                let urlString = url?.absoluteString
                //print(urlString)
                
                cell.vendorLogo.loadURL(urlString!)
                
                
            }else{
                print(error)
            }
        })
        
        return cell
       
    }
 
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//        if (segue.identifier == "goToRewardDetails"){
//            let destination = segue.destination as! RewardDetailsViewController
//            //  destination.delegate = self
//            destination.vendorName = rewardArray[position].vendorName
//                  destination.vendorAddess = rewardArray[position].vendorAddress
//                  destination.rewardId = rewardArray[position].key
//                  destination.offerName = rewardArray[position].rewardDesc
//
//        }
//
//    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        position  = indexPath.row
 
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "rewardDetails") as! RewardDetailsViewController
        destination.vendorName = rewardArray[position].vendorName
        destination.vendorAddess = rewardArray[position].vendorAddress
        destination.rewardId = rewardArray[position].key
        destination.offerName = rewardArray[position].rewardDesc
        present(destination, animated: true, completion: nil)
        
    }
  

}
