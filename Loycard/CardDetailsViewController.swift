//
//  CardDetailsViewController.swift
//  Loycard
//
//  Created by Forhad on 1/6/18.
//  Copyright © 2018 invertemotech. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseAuthUI
import Firebase
import FirebaseCore
import FirebaseDatabase
import FirebaseStorage
import EzImageLoader
import SCLAlertView





class CardDetailsViewController: UIViewController , PopUpDeligate {
    var rewardArray : [RewardModel] = [RewardModel]()
    let storage = Storage.storage()
    @IBOutlet weak var rewardInfoLabel: UILabel!
    var cardId = String()
    var vendorName = String()
    var offerID = String()
    var vendorID = String()
    
    var offerName = String()
    var offerReward = String()
    var vendorAddess = String()
    
    var purchasePerReward = String()
    var purchaseCount = String()
    var rewardIssued = String()
    var purchaseForNextReward = String()
    var userId : String = ""
    
    var purchaseToShow = Int ()
    
    var loyaltyCard = LoyaltyCardModel()
    
    @IBOutlet weak var offerNameLabel: UILabel!
    @IBOutlet weak var vendorNameLabel: UILabel!
    @IBOutlet weak var vendorAddresslabel: UILabel!
    @IBOutlet weak var perchasePerRewardLabel: UILabel!
    @IBOutlet weak var rewardDescLabel: UILabel!
    @IBOutlet weak var purchaseCountLabel: UILabel!
    @IBOutlet weak var rewardIssuedLabel: UILabel!
    @IBOutlet weak var purchaseToNextRewardLabel: UILabel!
    @IBOutlet weak var offerImageView: UIImageView!
    @IBOutlet weak var rewardPendingView: UIView!
    
    
    
    
    @IBOutlet weak var purchaseImage12: UIImageView!
    @IBOutlet weak var purchaseImage11: UIImageView!
    @IBOutlet weak var purchaseImage10: UIImageView!
    @IBOutlet weak var purchaseImage9: UIImageView!
    @IBOutlet weak var purchaseImage8: UIImageView!
    @IBOutlet weak var purchaseImage7: UIImageView!
    @IBOutlet weak var purchaseImage6: UIImageView!
    @IBOutlet weak var purchaseImage5: UIImageView!
    @IBOutlet weak var purchaseImage4: UIImageView!
    @IBOutlet weak var purchaseImage3: UIImageView!
    @IBOutlet weak var purchaseImage2: UIImageView!
    @IBOutlet weak var purchaseImage1: UIImageView!
    
    
    var numberOfPendingReward : Int = 0
    
    
    var currentUser : User? = Auth.auth().currentUser
    
    var token : Int = 1;
    
    
    
    
    func ButtonPressed(name: String) {
        
        if(name.elementsEqual("punch")){
            gotoPunch()
        }else if(name.elementsEqual("redeem")){
            gotoReward()
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if currentUser != nil{
            
            userId = (currentUser?.uid)!
        }
        
        self.rewardPendingView.isHidden = true
        
        
        print("cardId",cardId)
        setdata()
        
        
        retrieveMessages()
        
    }
    
    func setdata()   {
        offerNameLabel.text = offerName
        vendorNameLabel.text = vendorName
        vendorAddresslabel.text = vendorAddess
        rewardDescLabel.text = "Reward: \(offerReward)"
        perchasePerRewardLabel.text = "Purchase Per Reward: \(purchasePerReward)"
        
        offerImageView.image = UIImage.init(named: "logo.png")
        
        
        storage.reference().child("Images/\(offerID)/\(vendorID)").downloadURL(completion: { (
            url, error) in
            if(error == nil){
                
                let urlString = url?.absoluteString
                print(urlString)
                
                self.offerImageView.loadURL(urlString!)
                
            }else{
                print(error)
            }
        })
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        self.performSegue(withIdentifier: "goBack", sender: self)
        
        //  dismiss(animated: true, completion:  nil)
        // self.navigationController?.dismiss(animated: true, completion: nil)
    }
    @IBAction func punchPressed(_ sender: Any) {
        
        print("numberOfPendingReward",numberOfPendingReward)
        if(numberOfPendingReward>0){
  
       /*    let color = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
         //    let color = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            let appearance = SCLAlertView.SCLAppearance(
                
                kCircleTopPosition: 10.00, kCircleBackgroundTopPosition: 20, kCircleHeight: 55, kCircleIconHeight: 40, showCloseButton: false

            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("REDEEM"){
              self.gotoReward()
            }
            alertView.addButton("PUNCH") {
              self.gotoPunch()
            }
        
        
            alertView.showCustom("Attention", subTitle: "You have pending reward", color: color, icon: UIImage(named : "ic_reward_new.png")!)
 */
            
            openDialog()
            
        }else{
            gotoPunch()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func openDialog(){
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "fhPopUp") as! RewardPopUpViewController
   
 
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
    
        popOverVC.deligate = self
    }
    
 
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "goBack"){
            let destination = segue.destination as! MainTabViewController
            //  destination.delegate = self
            destination.previousPage = token
            
            
        }
        //        else if(segue.identifier == "goPunch"){
        //
        //            let destination = segue.destination as! PunchViewController
        //            destination.offerName = offerName
        //             destination.vendorAddess = vendorAddess
        //             destination.vendorName = vendorName
        //
        //        }
        
    }
    
    
    
    func gotoPunch(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "punchPage") as! PunchViewController
        destination.offerName = offerName
        destination.vendorAddess = vendorAddess
        destination.vendorName = vendorName
             destination.cardId = cardId
        
        
        present(destination, animated: true, completion: nil)
    }
    
    
    func gotoReward(){
        token = 2
        self.performSegue(withIdentifier: "goBack", sender: self)
    }
    
    func retrieveMessages(){
        
        
        
        let messageDB  = Database.database().reference().child("LoyaltyCards").child(cardId)
        
        
        messageDB.observe(.value, with: { (snapshot) in
            let snapShotValue = snapshot.value as? [String: Any]
            
            
            print(snapShotValue)
            
            
            
            let customerID = snapShotValue?["customerID"]! as! String
            let key = snapshot.key
            let offerID = snapShotValue?["offerID"]! as! String
            let offerID_customerID = snapShotValue?["offerID_customerID"]! as! String
            self.purchaseCount = snapShotValue?["purchaseCount"]! as! String
            self.purchasePerReward = snapShotValue?["purchasesPerReward"]! as! String
            let rewardDue = snapShotValue?["rewardDue"]! as! Bool
            let rewardsClaimed = snapShotValue?["rewardsClaimed"]! as! String
            self.rewardIssued = snapShotValue?["rewardsIssued"]! as! String
            let vendorID = snapShotValue?["vendorID"]! as! String
            
            
            
            self.loyaltyCard.customerID = customerID
            self.loyaltyCard.key = key
            self.loyaltyCard.offerID = offerID
            self.loyaltyCard.offerID_customerID = offerID_customerID
            self.loyaltyCard.purchaseCount = self.purchaseCount
            self.loyaltyCard.purchasesPerReward = self.purchasePerReward
            self.loyaltyCard.rewardDue = String(rewardDue)
            self.loyaltyCard.rewardsClaimed = rewardsClaimed
            self.loyaltyCard.rewardsIssued = self.rewardIssued
            self.loyaltyCard.vendorID = vendorID
            
            
            self.retrieveReward()
            
            self.setPurchaseInfo()
            
            
            
        }) { (error) in
            print(error)
            if error != nil{
                self.retrieveMessages()
            }
        }
        
        
        
        
    }
    
    
    
    func setPurchaseInfo(){
        
        purchaseCountLabel.text = "Purchase Count: \(purchaseCount)"
        rewardIssuedLabel.text = "Reward Issued: \(rewardIssued)"
        
        let  pc : Int = Int(purchaseCount)!
        let  ppr : Int = Int(purchasePerReward)!
        
        let pfnr: Int = ppr - (pc % ppr)
        
        self.purchaseToShow = pc % ppr
        
        purchaseToNextRewardLabel.text = "Purchase to Next Reward: \(pfnr)"
        
        
        
        setPuchaseToShow()
        showNextReward()
        
    }
    
    
    func setPuchaseToShow(){
        
        
        if(purchaseToShow == 0){
            purchaseImage1.image = UIImage.init(named: "ic_empty.png")
            purchaseImage2.image = UIImage.init(named: "ic_empty.png")
            purchaseImage3.image = UIImage.init(named: "ic_empty.png")
            purchaseImage4.image = UIImage.init(named: "ic_empty.png")
            purchaseImage5.image = UIImage.init(named: "ic_empty.png")
            purchaseImage6.image = UIImage.init(named: "ic_empty.png")
            purchaseImage7.image = UIImage.init(named: "ic_empty.png")
            purchaseImage8.image = UIImage.init(named: "ic_empty.png")
            purchaseImage9.image = UIImage.init(named: "ic_empty.png")
            purchaseImage10.image = UIImage.init(named: "ic_empty.png")
            purchaseImage11.image = UIImage.init(named: "ic_empty.png")
            purchaseImage12.image = UIImage.init(named: "ic_empty.png")
            
        }else if(purchaseToShow == 1){
            purchaseImage1.image = UIImage.init(named: "ic_punched.png")
            purchaseImage2.image = UIImage.init(named: "ic_empty.png")
            purchaseImage3.image = UIImage.init(named: "ic_empty.png")
            purchaseImage4.image = UIImage.init(named: "ic_empty.png")
            purchaseImage5.image = UIImage.init(named: "ic_empty.png")
            purchaseImage6.image = UIImage.init(named: "ic_empty.png")
            purchaseImage7.image = UIImage.init(named: "ic_empty.png")
            purchaseImage8.image = UIImage.init(named: "ic_empty.png")
            purchaseImage9.image = UIImage.init(named: "ic_empty.png")
            purchaseImage10.image = UIImage.init(named: "ic_empty.png")
            purchaseImage11.image = UIImage.init(named: "ic_empty.png")
            purchaseImage12.image = UIImage.init(named: "ic_empty.png")
            
        }else if(purchaseToShow == 2){
            purchaseImage1.image = UIImage.init(named: "ic_punched.png")
            purchaseImage2.image = UIImage.init(named: "ic_punched.png")
            purchaseImage3.image = UIImage.init(named: "ic_empty.png")
            purchaseImage4.image = UIImage.init(named: "ic_empty.png")
            purchaseImage5.image = UIImage.init(named: "ic_empty.png")
            purchaseImage6.image = UIImage.init(named: "ic_empty.png")
            purchaseImage7.image = UIImage.init(named: "ic_empty.png")
            purchaseImage8.image = UIImage.init(named: "ic_empty.png")
            purchaseImage9.image = UIImage.init(named: "ic_empty.png")
            purchaseImage10.image = UIImage.init(named: "ic_empty.png")
            purchaseImage11.image = UIImage.init(named: "ic_empty.png")
            purchaseImage12.image = UIImage.init(named: "ic_empty.png")
            
        }else if(purchaseToShow == 3){
            purchaseImage1.image = UIImage.init(named: "ic_punched.png")
            purchaseImage2.image = UIImage.init(named: "ic_punched.png")
            purchaseImage3.image = UIImage.init(named: "ic_punched.png")
            purchaseImage4.image = UIImage.init(named: "ic_empty.png")
            purchaseImage5.image = UIImage.init(named: "ic_empty.png")
            purchaseImage6.image = UIImage.init(named: "ic_empty.png")
            purchaseImage7.image = UIImage.init(named: "ic_empty.png")
            purchaseImage8.image = UIImage.init(named: "ic_empty.png")
            purchaseImage9.image = UIImage.init(named: "ic_empty.png")
            purchaseImage10.image = UIImage.init(named: "ic_empty.png")
            purchaseImage11.image = UIImage.init(named: "ic_empty.png")
            purchaseImage12.image = UIImage.init(named: "ic_empty.png")
            
        }else if(purchaseToShow == 4){
            purchaseImage1.image = UIImage.init(named: "ic_punched.png")
            purchaseImage2.image = UIImage.init(named: "ic_punched.png")
            purchaseImage3.image = UIImage.init(named: "ic_punched.png")
            purchaseImage4.image = UIImage.init(named: "ic_punched.png")
            purchaseImage5.image = UIImage.init(named: "ic_empty.png")
            purchaseImage6.image = UIImage.init(named: "ic_empty.png")
            purchaseImage7.image = UIImage.init(named: "ic_empty.png")
            purchaseImage8.image = UIImage.init(named: "ic_empty.png")
            purchaseImage9.image = UIImage.init(named: "ic_empty.png")
            purchaseImage10.image = UIImage.init(named: "ic_empty.png")
            purchaseImage11.image = UIImage.init(named: "ic_empty.png")
            purchaseImage12.image = UIImage.init(named: "ic_empty.png")
            
        }else if(purchaseToShow == 5){
            purchaseImage1.image = UIImage.init(named: "ic_punched.png")
            purchaseImage2.image = UIImage.init(named: "ic_punched.png")
            purchaseImage3.image = UIImage.init(named: "ic_punched.png")
            purchaseImage4.image = UIImage.init(named: "ic_punched.png")
            purchaseImage5.image = UIImage.init(named: "ic_punched.png")
            purchaseImage6.image = UIImage.init(named: "ic_empty.png")
            purchaseImage7.image = UIImage.init(named: "ic_empty.png")
            purchaseImage8.image = UIImage.init(named: "ic_empty.png")
            purchaseImage9.image = UIImage.init(named: "ic_empty.png")
            purchaseImage10.image = UIImage.init(named: "ic_empty.png")
            purchaseImage11.image = UIImage.init(named: "ic_empty.png")
            purchaseImage12.image = UIImage.init(named: "ic_empty.png")
            
        }else if(purchaseToShow == 6){
            purchaseImage1.image = UIImage.init(named: "ic_punched.png")
            purchaseImage2.image = UIImage.init(named: "ic_punched.png")
            purchaseImage3.image = UIImage.init(named: "ic_punched.png")
            purchaseImage4.image = UIImage.init(named: "ic_punched.png")
            purchaseImage5.image = UIImage.init(named: "ic_punched.png")
            purchaseImage6.image = UIImage.init(named: "ic_punched.png")
            purchaseImage7.image = UIImage.init(named: "ic_empty.png")
            purchaseImage8.image = UIImage.init(named: "ic_empty.png")
            purchaseImage9.image = UIImage.init(named: "ic_empty.png")
            purchaseImage10.image = UIImage.init(named: "ic_empty.png")
            purchaseImage11.image = UIImage.init(named: "ic_empty.png")
            purchaseImage12.image = UIImage.init(named: "ic_empty.png")
            
        }else if(purchaseToShow == 7){
            purchaseImage1.image = UIImage.init(named: "ic_punched.png")
            purchaseImage2.image = UIImage.init(named: "ic_punched.png")
            purchaseImage3.image = UIImage.init(named: "ic_punched.png")
            purchaseImage4.image = UIImage.init(named: "ic_punched.png")
            purchaseImage5.image = UIImage.init(named: "ic_punched.png")
            purchaseImage6.image = UIImage.init(named: "ic_punched.png")
            purchaseImage7.image = UIImage.init(named: "ic_punched.png")
            purchaseImage8.image = UIImage.init(named: "ic_empty.png")
            purchaseImage9.image = UIImage.init(named: "ic_empty.png")
            purchaseImage10.image = UIImage.init(named: "ic_empty.png")
            purchaseImage11.image = UIImage.init(named: "ic_empty.png")
            purchaseImage12.image = UIImage.init(named: "ic_empty.png")
            
        }else if(purchaseToShow == 8){
            purchaseImage1.image = UIImage.init(named: "ic_punched.png")
            purchaseImage2.image = UIImage.init(named: "ic_punched.png")
            purchaseImage3.image = UIImage.init(named: "ic_punched.png")
            purchaseImage4.image = UIImage.init(named: "ic_punched.png")
            purchaseImage5.image = UIImage.init(named: "ic_punched.png")
            purchaseImage6.image = UIImage.init(named: "ic_punched.png")
            purchaseImage7.image = UIImage.init(named: "ic_punched.png")
            purchaseImage8.image = UIImage.init(named: "ic_punched.png")
            purchaseImage9.image = UIImage.init(named: "ic_empty.png")
            purchaseImage10.image = UIImage.init(named: "ic_empty.png")
            purchaseImage11.image = UIImage.init(named: "ic_empty.png")
            purchaseImage12.image = UIImage.init(named: "ic_empty.png")
            
        }else if(purchaseToShow == 9){
            purchaseImage1.image = UIImage.init(named: "ic_punched.png")
            purchaseImage2.image = UIImage.init(named: "ic_punched.png")
            purchaseImage3.image = UIImage.init(named: "ic_punched.png")
            purchaseImage4.image = UIImage.init(named: "ic_punched.png")
            purchaseImage5.image = UIImage.init(named: "ic_punched.png")
            purchaseImage6.image = UIImage.init(named: "ic_punched.png")
            purchaseImage7.image = UIImage.init(named: "ic_punched.png")
            purchaseImage8.image = UIImage.init(named: "ic_punched.png")
            purchaseImage9.image = UIImage.init(named: "ic_punched.png")
            purchaseImage10.image = UIImage.init(named: "ic_empty.png")
            purchaseImage11.image = UIImage.init(named: "ic_empty.png")
            purchaseImage12.image = UIImage.init(named: "ic_empty.png")
            
        }else if(purchaseToShow == 10){
            purchaseImage1.image = UIImage.init(named: "ic_punched.png")
            purchaseImage2.image = UIImage.init(named: "ic_punched.png")
            purchaseImage3.image = UIImage.init(named: "ic_punched.png")
            purchaseImage4.image = UIImage.init(named: "ic_punched.png")
            purchaseImage5.image = UIImage.init(named: "ic_punched.png")
            purchaseImage6.image = UIImage.init(named: "ic_punched.png")
            purchaseImage7.image = UIImage.init(named: "ic_punched.png")
            purchaseImage8.image = UIImage.init(named: "ic_punched.png")
            purchaseImage9.image = UIImage.init(named: "ic_punched.png")
            purchaseImage10.image = UIImage.init(named: "ic_punched.png")
            purchaseImage11.image = UIImage.init(named: "ic_empty.png")
            purchaseImage12.image = UIImage.init(named: "ic_empty.png")
            
        }else if(purchaseToShow == 11){
            purchaseImage1.image = UIImage.init(named: "ic_punched.png")
            purchaseImage2.image = UIImage.init(named: "ic_punched.png")
            purchaseImage3.image = UIImage.init(named: "ic_punched.png")
            purchaseImage4.image = UIImage.init(named: "ic_punched.png")
            purchaseImage5.image = UIImage.init(named: "ic_punched.png")
            purchaseImage6.image = UIImage.init(named: "ic_punched.png")
            purchaseImage7.image = UIImage.init(named: "ic_punched.png")
            purchaseImage8.image = UIImage.init(named: "ic_punched.png")
            purchaseImage9.image = UIImage.init(named: "ic_punched.png")
            purchaseImage10.image = UIImage.init(named: "ic_punched.png")
            purchaseImage11.image = UIImage.init(named: "ic_punched.png")
            purchaseImage12.image = UIImage.init(named: "ic_empty.png")
            
        }else if(purchaseToShow == 12){
            purchaseImage1.image = UIImage.init(named: "ic_punched.png")
            purchaseImage2.image = UIImage.init(named: "ic_punched.png")
            purchaseImage3.image = UIImage.init(named: "ic_punched.png")
            purchaseImage4.image = UIImage.init(named: "ic_punched.png")
            purchaseImage5.image = UIImage.init(named: "ic_punched.png")
            purchaseImage6.image = UIImage.init(named: "ic_punched.png")
            purchaseImage7.image = UIImage.init(named: "ic_punched.png")
            purchaseImage8.image = UIImage.init(named: "ic_punched.png")
            purchaseImage9.image = UIImage.init(named: "ic_punched.png")
            purchaseImage10.image = UIImage.init(named: "ic_punched.png")
            purchaseImage11.image = UIImage.init(named: "ic_punched.png")
            purchaseImage12.image = UIImage.init(named: "ic_punched.png")
            
        }
        
    }
    
    
    func showNextReward(){
        
        let x = Int(purchasePerReward)
        
        if(x == 1){
            purchaseImage1.image = UIImage.init(named: "ic_reward.png")
        }else if(x == 2){
            purchaseImage2.image = UIImage.init(named: "ic_reward.png")
        }else if(x == 3){
            purchaseImage3.image = UIImage.init(named: "ic_reward.png")
        }else if(x == 4){
            purchaseImage4.image = UIImage.init(named: "ic_reward.png")
        }else if(x == 5){
            purchaseImage5.image = UIImage.init(named: "ic_reward.png")
        }else if(x == 6){
            purchaseImage6.image = UIImage.init(named: "ic_reward.png")
        }else if(x == 7){
            purchaseImage7.image = UIImage.init(named: "ic_reward.png")
        }else if(x == 8){
            purchaseImage8.image = UIImage.init(named: "ic_reward.png")
        }else if(x == 9){
            purchaseImage9.image = UIImage.init(named: "ic_reward.png")
        }else if(x == 10){
            purchaseImage10.image = UIImage.init(named: "ic_reward.png")
        }else if(x == 11){
            purchaseImage11.image = UIImage.init(named: "ic_reward.png")
        }else if(x == 12){
            purchaseImage12.image = UIImage.init(named: "ic_reward.png")
        }
    }
    
    
    func retrieveReward(){
        
        rewardArray = [RewardModel]()
        
        let cardID_customerID = cardId+"_"+userId
        
        let messageDB  = Database.database().reference().child("LoyaltyRewards").queryOrdered(byChild: "cardID_customerID").queryEqual(toValue: cardID_customerID)
        
        
        messageDB.observe(.childAdded, with: { (snapshot) in
            let snapShotValue = snapshot.value as? [String: Any]
            
            let rewardModel = RewardModel ()
            let key = snapshot.key
            let customerID = snapShotValue?["customerID"]! as! String
            let cardID_customerID = snapShotValue?["cardID_customerID"]! as! String
            let offerID = snapShotValue?["offerID"]! as! String
            let rewardDesc = snapShotValue?["rewardDesc"]! as! String
            let rewardsAvailable = snapShotValue?["rewardsAvailable"]! as! String
            let vendorID = snapShotValue?["vendorID"]! as! String
            rewardModel.customerID = customerID
            rewardModel.key = key
            rewardModel.offerID = offerID
            rewardModel.cardID_customerID = cardID_customerID
            rewardModel.rewardDesc = rewardDesc
            rewardModel.rewardsAvailable = rewardsAvailable
            rewardModel.vendorID = vendorID
            self.rewardArray.append(rewardModel)
            
            self.checkReward()
            
        }) { (error) in
            print(error)
            if error != nil{
                self.retrieveReward()
            }
        }
        
    }
    
    
    func checkReward(){
        
        numberOfPendingReward = rewardArray.count
        
        if(rewardArray.count > 0 ){
            self.rewardPendingView.isHidden = false
            if(rewardArray.count == 1){
                self.rewardInfoLabel.text = "You have \(rewardArray.count) pending reward"
            }else{
                self.rewardInfoLabel.text = "You have \(rewardArray.count) pending rewards"
            }
        }else{
            self.rewardPendingView.isHidden = true
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
