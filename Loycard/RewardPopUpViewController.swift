//
//  RewardPopUpViewController.swift
//  Loycard
//
//  Created by Forhad on 1/17/18.
//  Copyright © 2018 invertemotech. All rights reserved.
//

import UIKit


protocol PopUpDeligate {
    
    func  ButtonPressed(name : String)
}


class RewardPopUpViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var subtitleLabel: UILabel!
 
        var deligate : PopUpDeligate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.mainView.layer.cornerRadius = 8
        self.mainView.clipsToBounds = true
        
        self.showAnimate()
        titleLabel.text = "Attention!!"
          subtitleLabel.text = "You have pending reward"
    }
    
    @IBAction func punchBtnPressed(_ sender: Any) {
  
        
        deligate?.ButtonPressed(name: "punch")
        removeAnimate()
    }
    
    @IBAction func redeemPressed(_ sender: Any) {
        
         deligate?.ButtonPressed(name: "redeem")
        removeAnimate()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
