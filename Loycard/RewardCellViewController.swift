//
//  RewardCellViewControllerTableViewCell.swift
//  Loycard
//
//  Created by Forhad on 1/2/18.
//  Copyright © 2018 invertemotech. All rights reserved.
//

import UIKit

class RewardCellViewController: UITableViewCell {
    @IBOutlet weak var vendorName: UILabel!

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var rewardLebel: UILabel!
    @IBOutlet weak var vendorLogo: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
