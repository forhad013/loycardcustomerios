//
//  SignInViewController.swift
//  Loycard
//
//  Created by Forhad on 12/21/17.
//  Copyright © 2017 invertemotech. All rights reserved.
//

import UIKit
import GoogleSignIn
import Firebase

class SignInViewController: UIViewController ,GIDSignInUIDelegate ,GIDSignInDelegate{

    @IBOutlet weak var gmailSignInLayout: UIView!
    @IBOutlet weak var emailSignInButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
 
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
  
        
        
        
        
        let googleSignInBtn = GIDSignInButton(frame : CGRect(x: 0, y: 0, width: 200, height: 50))
        googleSignInBtn.style = GIDSignInButtonStyle.wide
        self.gmailSignInLayout.addSubview(googleSignInBtn)
        
   
    
    }

    @IBAction func emailSignInBtnPressed(_ sender: Any) {
        
            self.performSegue(withIdentifier: "goToEmailSignIn", sender: self)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    
    }
    
    
    
 
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        
        if(error != nil){
            print(error)
        }
          print(user.profile.email)
        
        self.performSegue(withIdentifier: "goToQrScanner", sender: self)
        
        
    }
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
