//
//  LogoutViewController.swift
//  Loycard
//
//  Created by Forhad on 12/26/17.
//  Copyright © 2017 invertemotech. All rights reserved.
//

import UIKit

import FirebaseAuth
import Firebase

class LogoutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.]
        
        let alert = UIAlertController(title: "Log Out", message: "Are you sure?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default,  handler: { action in
            print("Click of yes button")
            do {
                try
                    Auth.auth().signOut()
                self.viewDidAppear(true)
                self.performSegue(withIdentifier: "logout", sender: self)
            } catch {
                print("Something wrong")
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel,  handler: { action in
            print("Click of yes button")
            alert.dismiss(animated: true, completion: nil)
                     self.performSegue(withIdentifier: "logout", sender: self)
         
        }))
        self.present(alert, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
