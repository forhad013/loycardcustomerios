//
//  TutorialController.swift
//  Loycard
//
//  Created by Forhad on 12/20/17./Volumes/Mixed/IOS Project/Loycard/Podfile
//  Copyright © 2017 invertemotech. All rights reserved.
//

import UIKit

class TutorialController: UIViewController {
 
    @IBAction func startTourPressed(_ sender: Any) {
          self.performSegue(withIdentifier: "startTour", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
