//
//  PromotionTableViewController.swift
//  Loycard
//
//  Created by Forhad on 1/8/18.
//  Copyright © 2018 invertemotech. All rights reserved.
//

import UIKit
import FirebaseAuth

import Firebase
import FirebaseCore
import FirebaseDatabase
import FirebaseStorage
import EzImageLoader

class PromotionTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate  {
    
    
    var voucherArray : [String] = [String]()
    
    var position = Int()
    let storage = Storage.storage()
    var promotionArray : [PromotionModel] = [PromotionModel]()
    
    var currentUser : User? = Auth.auth().currentUser
    
    var i : Int = 0
    
    @IBOutlet weak var tableView: UITableView!
    var userId : String = ""
    var ref: DatabaseReference!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
       
        
        let storageRef = storage.reference()
        
        if currentUser != nil{
            
            userId = (currentUser?.uid)!
        }
        
        
        
        retrieveVouchers()
       // tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    func retrieveVouchers(){
        
        
        
        let messageDB  = Database.database().reference().child("Vouchers").queryOrdered(byChild: "customerID").queryEqual(toValue: userId)
        
        
        //        messageDB.observe(.childAdded, with: {(snapshot) in
        //
        //            let snapShotValue = snapshot.value as? [String: Any]
        //
        //            let key = snapshot.key
        //            let promoID = snapShotValue?["promoID"]! as! String
        //            self.voucherArray.append(promoID)
        //
        //            self.getPromotions()
        //
        //        }) { (error) in
        //            print(error)
        //            if error != nil{
        //                self.retrieveVouchers()
        //            }
        //        }
        
        messageDB.observe(.value, with: { (snapshot) in
            if snapshot.exists() {
            
                
                if let snapDict = snapshot.value as? [String:AnyObject]{
                    for each in snapDict{
                        let snapShotValue = each.value as? [String: Any]
                        
                        print(snapShotValue)
                        
                         let promoID = snapShotValue?["promoID"]! as! String
                         self.voucherArray.append(promoID)
                    }
                    
                    self.getPromotions()
                    
                }
                
            }
            else {
                print ("snapshot doesn't exist")
            }
        }){ (error) in
            print(error)
            if error != nil{
                self.retrieveVouchers()
            }
        }
        
        
        
    }
    
    
    func getPromotions(){
        
      //  print("pro")
        
        promotionArray = [PromotionModel]()
        
        var position : Int = 0
        
        for item in voucherArray{
            
            let messageDB  = Database.database().reference().child("Promotions").child(item)
            
            
            
            messageDB.observe(.value, with: { (snapshot) in
                let snapShotValue = snapshot.value as? [String: Any]
                
                let creationDate = snapShotValue?["creationDate"]! as! String
                let description = snapShotValue?["description"]! as! String
                let expiryDate = snapShotValue?["expiryDate"]! as! String
                let title = snapShotValue?["title"]! as! String
                let vendorId = snapShotValue?["vendorId"]! as! String
                
                
                
                let promotion = PromotionModel()
                
                promotion.creationDate = creationDate
                promotion.description = description
                promotion.expiryDate = expiryDate
                promotion.title = title
                promotion.vendorId = vendorId
                
                self.promotionArray.append(promotion)
                
                
                self.retrieveVendorDetails(vendorID: vendorId, i: position)
                position = position+1
                // self.tableView.reloadData()
                
                
            }) { (error) in
                if(error != nil){
                    self.getPromotions()
                }
            }
            
        }
    }
    
    
    
    
    func retrieveVendorDetails(vendorID :String , i : Int){
        
        
        
        let messageDB  = Database.database().reference().child("Vendors").child(vendorID)
        
        
        messageDB.observeSingleEvent(of: .value, with: { (snapshot) in
            let snapShotValue = snapshot.value as? [String: Any]
            
            let vendorName = snapShotValue?["businessName"]! as! String
            let businessAddress = snapShotValue?["businessAddress"]! as! String
            self.promotionArray[i].vendorName = vendorName
            self.promotionArray[i].vendorAddress = businessAddress
            
            
            self.tableView.reloadData()
            
            
        }) { (error) in
            if(error != nil){
                self.retrieveVendorDetails(vendorID: vendorID, i: self.i)
            }
        }
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
      func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return promotionArray.count
    }
    
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "promotionCell", for: indexPath) as! PromotionTableViewCell
        
        
        //        cell.mainView.layer.cornerRadius = 8
        //        cell.clipsToBounds = true
        
        cell.vendorNameLabel.text = promotionArray[indexPath.row].vendorName
        cell.vendorAddressLabel.text = promotionArray[indexPath.row].vendorAddress
        cell.title.text = promotionArray[indexPath.row].title
        cell.expireDateLabel.text = "Expires \(promotionArray[indexPath.row].expiryDate)"
        cell.descriptionLabel.text = promotionArray[indexPath.row].description
     
        return cell
        
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
