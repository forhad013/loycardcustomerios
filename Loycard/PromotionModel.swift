//
//  PromotionModel.swift
//  Loycard
//
//  Created by Forhad on 1/8/18.
//  Copyright © 2018 invertemotech. All rights reserved.
//

import Foundation

class PromotionModel
{
    
    var key : String = ""
    var creationDate : String = ""
    var description : String = ""
    var expiryDate : String = ""
    var title : String = ""
    var vendorId : String = ""
    var vendorName : String = ""
    var vendorAddress : String = ""
    
}
