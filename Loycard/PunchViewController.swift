//
//  PunchViewController.swift
//  Loycard
//
//  Created by Forhad on 1/8/18.
//  Copyright © 2018 invertemotech. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseAuthUI
import Firebase
import FirebaseCore
import FirebaseDatabase
import FirebaseStorage
import EzImageLoader
import QRCode
import SCLAlertView

class PunchViewController: UIViewController {
    
    var cardId = String()
    var vendorName = String()
    
    
    var offerName = String()
    
    var vendorAddess = String()
    var userId = String()
    var firstTime = true
    
    @IBOutlet weak var qrImageView: UIImageView!

    @IBOutlet weak var vendorNameLabel: UILabel!
    @IBOutlet weak var vendorAddressLabel: UILabel!
    @IBOutlet weak var offerNameLabel: UILabel!
    
    var currentUser : User? = Auth.auth().currentUser
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if currentUser != nil{
            
            userId = (currentUser?.uid)!
            setData()
        }
        
        retrieveMessages()
        
        // Do any additional setup after loading the view.
    }
    
    
    func setData(){
        offerNameLabel.text = offerName
        vendorNameLabel.text = vendorName
        vendorAddressLabel.text = vendorAddess
        
        
        
      //  generateQrImage()
        
        qrImageView.image = {
            var qrCode = QRCode(userId)!
            qrCode.size = self.qrImageView.bounds.size
            qrCode.errorCorrection = .High
            return qrCode.image
        }()
        
    }
    
    
    func retrieveMessages(){
        
        
        
        let messageDB  = Database.database().reference().child("LoyaltyCards").child(cardId)
        
        
        messageDB.observe(.value, with: { (snapshot) in
            let snapShotValue = snapshot.value as? [String: Any]
            
            
            print("firsttime" , self.firstTime)
            
            if(self.firstTime){
                self.firstTime = false
            }else{
             self.showDialog()
            }
            print("firsttime" , self.firstTime)
            
            
        }) { (error) in
            print(error)
            if error != nil{
                self.retrieveMessages()
            }
        }
        
    }
    
    
    
    func showDialog(){
        
        let color = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        let appearance = SCLAlertView.SCLAppearance(
            kCircleTopPosition: 2.00, kCircleHeight: 55, kCircleIconHeight: 40, showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("OK"){
            self.dismiss(animated: true, completion: nil)
        }
        alertView.showCustom("Congratulation!!", subTitle: "Purchase Count Successfully Updated", color: color, icon: UIImage(named : "ic_success_white.png")!)
        
     //   alertView.showSuccess("Congratulation!", subTitle: "Purchase Count Successfully Updated")
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func generateQrImage()  {
        
        let data = userId.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        filter?.setValue(data, forKey: "inputMessage")
        filter!.setValue("Q", forKey: "inputCorrectionLevel")
        
        let  qrcodeImage: CIImage   = filter!.outputImage!
        
        
        displayQRCodeImage(qrcodeImage: qrcodeImage)
        
        // ProgressHUD.dismiss()
        
    }
    
    func displayQRCodeImage(qrcodeImage: CIImage) {
        
        
       // let transformedImage = qrcodeImage.applying(CGAffineTransform(scaleX: 400, y: 400))
        
        qrImageView.image = UIImage.init(ciImage: qrcodeImage)
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
         dismiss(animated: true, completion:  nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
