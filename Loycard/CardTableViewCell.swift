//
//  CardTableViewCell.swift
//  Loycard
//
//  Created by Forhad on 12/28/17.
//  Copyright © 2017 invertemotech. All rights reserved.
//

import UIKit

class CardTableViewCell: UITableViewCell {

    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var purchaseNumber: UILabel!
    @IBOutlet weak var cardName: UILabel!
    @IBOutlet weak var vedorName: UILabel!
    override func awakeFromNib() {
   
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
