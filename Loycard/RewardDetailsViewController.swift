//
//  RewardDetailsViewController.swift
//  Loycard
//
//  Created by Forhad on 1/8/18.
//  Copyright © 2018 invertemotech. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseAuthUI
import Firebase
import FirebaseCore
import FirebaseDatabase
import FirebaseStorage
import EzImageLoader
import QRCode
import SCLAlertView

class RewardDetailsViewController: UIViewController {

    
    var rewardId = String()
    var vendorName = String()
   
    var firstTime = true
    var offerName = String()
 
    var vendorAddess = String()
    
    
    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var offerDescLabel: UILabel!
    @IBOutlet weak var vendorNameLabel: UILabel!
    @IBOutlet weak var vendorAddressLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        offerDescLabel.text = offerName
        vendorNameLabel.text = vendorName
        vendorAddressLabel.text = vendorAddess
        
        print("rewardId",rewardId)
        retrieveMessages()
      //  generateQrImage()
        
        qrImageView.image = {
            var qrCode = QRCode(rewardId)!
            qrCode.size = self.qrImageView.bounds.size
            qrCode.errorCorrection = .High
            return qrCode.image
        }()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func retrieveMessages(){
        
        
        
        let messageDB  = Database.database().reference().child("LoyaltyRewards").child(rewardId)
        
        
        messageDB.observe(.value, with: { (snapshot) in
            let snapShotValue = snapshot.value as? [String: Any]
            
            
            print("firsttime" , snapShotValue)
            
            if(self.firstTime){
                self.firstTime = false
            }else{
                if (snapShotValue == nil){
                    self.showDialog()
                }
            }
            print("firsttime" , self.firstTime)
            
            
        }) { (error) in
            print(error)
            if error != nil{
                self.retrieveMessages()
            }
        }
        
    }
    
    
    
    func showDialog(){
        
        let color = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        let appearance = SCLAlertView.SCLAppearance(
            kCircleTopPosition: 2.00, kCircleHeight: 55, kCircleIconHeight: 40, showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("OK"){
            self.dismiss(animated: true, completion: nil)
        }
        alertView.showCustom("Congratulation!!", subTitle: "Reward Successfully Redeemed", color: color, icon: UIImage(named : "ic_success_white.png")!)
        
        //   alertView.showSuccess("Congratulation!", subTitle: "Purchase Count Successfully Updated")
        
    }
    
    func generateQrImage()  {
        
        let data = rewardId.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        filter?.setValue(data, forKey: "inputMessage")
        filter!.setValue("Q", forKey: "inputCorrectionLevel")
        
        let  qrcodeImage: CIImage   = filter!.outputImage!
        
        
        displayQRCodeImage(qrcodeImage: qrcodeImage)
        
        // ProgressHUD.dismiss()
        
    }
    
    func displayQRCodeImage(qrcodeImage: CIImage) {
        
        
        let transformedImage = qrcodeImage.applying(CGAffineTransform(scaleX: 400, y: 400))
        
        qrImageView.image = UIImage.init(ciImage: transformedImage)
    }

    @IBAction func backBtnPressed(_ sender: Any) {
            self.performSegue(withIdentifier: "goBackFromReward", sender: self)
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "goBackFromReward"){
            let destination = segue.destination as! MainTabViewController
            //  destination.delegate = self
            destination.previousPage = 2
            
            
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
