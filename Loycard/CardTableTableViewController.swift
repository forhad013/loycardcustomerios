//
//  CardTableTableViewController.swift
//  Loycard
//
//  Created by Forhad on 12/28/17.
//  Copyright © 2017 invertemotech. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseAuthUI
import Firebase
import FirebaseCore
import FirebaseDatabase
import FirebaseStorage
import EzImageLoader




class CardTableTableViewController: UITableViewController {
    
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var position  = Int()
    
    let storage = Storage.storage()
    var loyaltyCardArray : [LoyaltyCardModel] = [LoyaltyCardModel]()
    
    var currentUser : User? = Auth.auth().currentUser
    
    var i : Int = 0
    
    var userId : String = ""
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        tableView.contentInset.top = 20
        let storageRef = storage.reference()
        
        if currentUser != nil{
            
            userId = (currentUser?.uid)!
        }
        
        retrieveMessages()
        
        
    }
    
    
    func retrieveMessages(){
        
        
        
        let messageDB  = Database.database().reference().child("LoyaltyCards").queryOrdered(byChild: "customerID").queryEqual(toValue: userId)
        
        
        messageDB.observe(.childAdded, with: { (snapshot) in
            let snapShotValue = snapshot.value as? [String: Any]
            
            
            
            
            let loyaltyCard = LoyaltyCardModel ()
            
            let customerID = snapShotValue?["customerID"]! as! String
            let key = snapshot.key
            let offerID = snapShotValue?["offerID"]! as! String
            let offerID_customerID = snapShotValue?["offerID_customerID"]! as! String
            let purchaseCount = snapShotValue?["purchaseCount"]! as! String
            let purchasesPerReward = snapShotValue?["purchasesPerReward"]! as! String
            let rewardDue = snapShotValue?["rewardDue"]! as! Bool
            let rewardsClaimed = snapShotValue?["rewardsClaimed"]! as! String
            let rewardsIssued = snapShotValue?["rewardsIssued"]! as! String
            let vendorID = snapShotValue?["vendorID"]! as! String
            
            
            
            loyaltyCard.customerID = customerID
            loyaltyCard.key = key
            loyaltyCard.offerID = offerID
            loyaltyCard.offerID_customerID = offerID_customerID
            loyaltyCard.purchaseCount = purchaseCount
            loyaltyCard.purchasesPerReward = purchasesPerReward
            loyaltyCard.rewardDue = String(rewardDue)
            loyaltyCard.rewardsClaimed = rewardsClaimed
            loyaltyCard.rewardsIssued = rewardsIssued
            loyaltyCard.vendorID = vendorID
            
            self.loyaltyCardArray.append(loyaltyCard)
            
            
            self.retrieveVendorDetails(vendorID: vendorID, offerID: offerID,i: self.i)
            
            
        }) { (error) in
            print(error)
            if error != nil{
                self.retrieveMessages()
            }
        }
        
        
        
        
    }
    
    func retrieveVendorDetails(vendorID :String , offerID: String, i : Int){
        
        
        let messageDB  = Database.database().reference().child("Vendors").child(vendorID)
        
        
        messageDB.observe(.value, with: { (snapshot) in
            let snapShotValue = snapshot.value as? [String: Any]
            
            let vendorName = snapShotValue?["businessName"]! as! String
            let vendorAddress = snapShotValue?["businessAddress"]! as! String
            print(vendorName)
            self.loyaltyCardArray[i].vendorName = vendorName
            self.loyaltyCardArray[i].vendorAddress = vendorAddress
            
        }) { (error) in
            if(error != nil){
                self.retrieveVendorDetails(vendorID: vendorID, offerID: offerID,i: self.i)
            }
        }
        self.retrieveOfferDetails(offerID: offerID,x: i)
    }
    
    
    
    func retrieveOfferDetails(offerID :String , x : Int){
        
        
        
        let messageDB  = Database.database().reference().child("LoyaltyOffers").child(offerID)
        messageDB.observe(.value, with: { (snapshot) in
            let snapShotValue = snapshot.value as? [String: Any]
            
            let offerName = snapShotValue?["description"]! as! String
            let offerReward = snapShotValue?["reward"]! as! String
            print(offerName)
            self.loyaltyCardArray[x].offerName = offerName
            self.loyaltyCardArray[x].offerReward = offerReward
            self.i = self.i+1
            
            self.tableView.reloadData()
            
        }) { (error) in
            if(error != nil){
                self.retrieveOfferDetails(offerID: offerID,x: x)
            }
        }
        
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return loyaltyCardArray.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cardCell", for: indexPath) as! CardTableViewCell
        
        // cell.cardImage.image = UIImage(named: "AppIcon.png")
        
        cell.cardName.text = loyaltyCardArray[indexPath.row].offerName
        cell.vedorName.text = loyaltyCardArray[indexPath.row].vendorName
        
        let offerID = loyaltyCardArray[indexPath.row].offerID
        let vendorID = loyaltyCardArray[indexPath.row].vendorID
        //        let pathReference = storage.reference(withPath: "Images/\(offerID)/\(vendorID)")
        
        cell.cardImage.image = UIImage.init(named: "logo.png")
        
        
        storage.reference().child("Images/\(offerID)/\(vendorID)").downloadURL(completion: { (
            url, error) in
            if(error == nil){
                
                let urlString = url?.absoluteString
                //print(urlString)
                
                cell.cardImage.loadURL(urlString!)
                
                
            }else{
                print(error)
            }
        })
        
        
        
        
        cell.purchaseNumber.text = "Purchases : \(loyaltyCardArray[indexPath.row].purchaseCount)"
        
        
        return cell
    }
    
    
    
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "goToDetails"){
            let destination = segue.destination as! CardDetailsViewController
            //  destination.delegate = self
            destination.cardId = loyaltyCardArray[position].key
            destination.vendorName = loyaltyCardArray[position].vendorName
            destination.offerID = loyaltyCardArray[position].offerID
            destination.vendorID = loyaltyCardArray[position].vendorID
            
            destination.offerName = loyaltyCardArray[position].offerName
            destination.offerReward = loyaltyCardArray[position].offerReward
            destination.vendorAddess = loyaltyCardArray[position].vendorAddress
            destination.purchasePerReward = loyaltyCardArray[position].purchasesPerReward
            
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        position  = indexPath.row
        print("position :",position)
        self.performSegue(withIdentifier: "goToDetails", sender: self)
    }
    
}

//extension CardTableTableViewController: UISearchResultsUpdating {
//    // MARK: - UISearchResultsUpdating Delegate
//    func updateSearchResults(for searchController: UISearchController) {
//
//    }
//}

