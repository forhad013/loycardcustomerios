//
//  MainTabViewController.swift
//  Loycard
//
//  Created by Forhad on 12/26/17.
//  Copyright © 2017 invertemotech. All rights reserved.
//

import Foundation
import UIKit



class MainTabViewController: UITabBarController  {
    
    
 
    var previousPage = Int()
    
  
  
    override func viewDidLoad() {
        super.viewDidLoad()
   
        
       // UIApplication.shared.statusBarStyle = #colorLiteral(red: 0.05098039216, green: 0.3647058824, blue: 0.5921568627, alpha: 1)
        
//         self.tabBar.barTintColor = #colorLiteral(red: 0.05098039216, green: 0.3647058824, blue: 0.5921568627, alpha: 1)
    
        if(previousPage == 1){
            self.selectedIndex = 1
        }else if(previousPage == 2){
             self.selectedIndex = 2
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        UINavigationBar.appearance().barStyle = .blackOpaque
        return true
    }
    
   
    
}
