/* 
Copyright (c) 2017 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct LoyaltyCard : Codable {
	let customerID : String?
	let offerID : String?
	let offerID_customerID : String?
	let purchaseCount : Int?
	let purchasesPerReward : Int?
	let rewardDue : Bool?
	let rewardsClaimed : Int?
	let rewardsIssued : Int?
	let vendorID : String?

	enum CodingKeys: String, CodingKey {

		case customerID = "customerID"
		case offerID = "offerID"
		case offerID_customerID = "offerID_customerID"
		case purchaseCount = "purchaseCount"
		case purchasesPerReward = "purchasesPerReward"
		case rewardDue = "rewardDue"
		case rewardsClaimed = "rewardsClaimed"
		case rewardsIssued = "rewardsIssued"
		case vendorID = "vendorID"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		customerID = try values.decodeIfPresent(String.self, forKey: .customerID)
		offerID = try values.decodeIfPresent(String.self, forKey: .offerID)
		offerID_customerID = try values.decodeIfPresent(String.self, forKey: .offerID_customerID)
		purchaseCount = try values.decodeIfPresent(Int.self, forKey: .purchaseCount)
		purchasesPerReward = try values.decodeIfPresent(Int.self, forKey: .purchasesPerReward)
		rewardDue = try values.decodeIfPresent(Bool.self, forKey: .rewardDue)
		rewardsClaimed = try values.decodeIfPresent(Int.self, forKey: .rewardsClaimed)
		rewardsIssued = try values.decodeIfPresent(Int.self, forKey: .rewardsIssued)
		vendorID = try values.decodeIfPresent(String.self, forKey: .vendorID)
	}

}
