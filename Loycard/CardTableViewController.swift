//
//  CardTableViewController.swift
//  Loycard
//
//  Created by Forhad on 1/8/18.
//  Copyright © 2018 invertemotech. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseAuthUI
import Firebase
import FirebaseCore
import FirebaseDatabase
import FirebaseStorage
import EzImageLoader

class CardTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate ,UISearchResultsUpdating {

    @IBOutlet var tableView: UITableView!
     
     let searchController = UISearchController(searchResultsController: nil)
    
    var tablePosition  = Int()
    
    let storage = Storage.storage()
    var loyaltyCardArray : [LoyaltyCardModel] = [LoyaltyCardModel]()
    var filteredloyaltyCardArray : [LoyaltyCardModel] = [LoyaltyCardModel]()
    var currentUser : User? = Auth.auth().currentUser
    
    var i : Int = 0
    
    var userId : String = ""
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let storageRef = storage.reference()
        
        if currentUser != nil{
            
            userId = (currentUser?.uid)!
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        
        retrieveMessages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        // If we haven't typed anything into the search bar then do not filter the results
        if searchController.searchBar.text! == "" {
            filteredloyaltyCardArray = loyaltyCardArray
        } else {
            // Filter the results
            filteredloyaltyCardArray = loyaltyCardArray.filter { $0.vendorName.lowercased().contains(searchController.searchBar.text!.lowercased()) ||
            $0.offerName.lowercased().contains(searchController.searchBar.text!.lowercased()) }
        }

        self.tableView.reloadData()
    }

    
    
    func retrieveMessages(){
        
        
        
        
        let messageDB  = Database.database().reference().child("LoyaltyCards").queryOrdered(byChild: "customerID").queryEqual(toValue: userId)
        
        
        
        messageDB.observe(.childAdded, with: { (snapshot) in
            let snapShotValue = snapshot.value as? [String: Any]
            
//            print("snapshot",snapshot)
//
//            print("snapshotValue",snapShotValue)
            
            
            let loyaltyCard = LoyaltyCardModel ()
            
            let customerID = snapShotValue?["customerID"]! as! String
            let key = snapshot.key
            let offerID = snapShotValue?["offerID"]! as! String
            let offerID_customerID = snapShotValue?["offerID_customerID"]! as! String
            let purchaseCount = snapShotValue?["purchaseCount"]! as! String
            let purchasesPerReward = snapShotValue?["purchasesPerReward"]! as! String
            let rewardDue = snapShotValue?["rewardDue"]! as! Bool
            let rewardsClaimed = snapShotValue?["rewardsClaimed"]! as! String
            let rewardsIssued = snapShotValue?["rewardsIssued"]! as! String
            let vendorID = snapShotValue?["vendorID"]! as! String
            
            
            
            loyaltyCard.customerID = customerID
            loyaltyCard.key = key
            loyaltyCard.offerID = offerID
            loyaltyCard.offerID_customerID = offerID_customerID
            loyaltyCard.purchaseCount = purchaseCount
            loyaltyCard.purchasesPerReward = purchasesPerReward
            loyaltyCard.rewardDue = String(rewardDue)
            loyaltyCard.rewardsClaimed = rewardsClaimed
            loyaltyCard.rewardsIssued = rewardsIssued
            loyaltyCard.vendorID = vendorID
            
            self.loyaltyCardArray.append(loyaltyCard)
            
            
         //   self.retrieveVendorDetails(vendorID: vendorID, offerID: offerID,z: self.i)
           self.getOtherInfo();
            
        }) { (error) in
            print(error)
            if error != nil{
                self.retrieveMessages()
            }
        }
        
    }
    
    
    func getOtherInfo(){
        
        var position : Int = 0
        
        for item in loyaltyCardArray{
            
        retrieveOfferDetails(offerID: item.offerID, x: position)
        retrieveVendorDetails(vendorID: item.vendorID, offerID: item.offerID,z: position)
        position = position + 1
            
        }
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func retrieveVendorDetails(vendorID :String , offerID: String, z : Int){
        
        
        let messageDB  = Database.database().reference().child("Vendors").child(vendorID)
        
 
        
        messageDB.observeSingleEvent(of: .value, with: { (snapshot) in
            
            let snapShotValue = snapshot.value as? [String: Any]
            
            let vendorName = snapShotValue?["businessName"]! as! String
            let vendorAddress = snapShotValue?["businessAddress"]! as! String
            //(vendorName)
            self.loyaltyCardArray[z].vendorName = vendorName
            self.loyaltyCardArray[z].vendorAddress = vendorAddress
            
        })  { (error) in
            if(error != nil){
                self.retrieveVendorDetails(vendorID: vendorID, offerID: offerID,z: self.i)
                //  self.retrieveOfferDetails(offerID: offerID,x: z)
            }
        }
        
    }
    
    
    
    func retrieveOfferDetails(offerID :String , x : Int){
        
        
        
        let messageDB  = Database.database().reference().child("LoyaltyOffers").child(offerID)
         messageDB.observeSingleEvent(of: .value, with: { (snapshot) in
            let snapShotValue = snapshot.value as? [String: Any]
            
            let offerName = snapShotValue?["description"]! as! String
            let offerReward = snapShotValue?["reward"]! as! String
         //   print(offerName)
            self.loyaltyCardArray[x].offerName = offerName
            self.loyaltyCardArray[x].offerReward = offerReward
            //self.i = self.i+1
            
            
            self.filteredloyaltyCardArray = self.loyaltyCardArray
            
            self.tableView.reloadData()
            
        }) { (error) in
            if(error != nil){
                self.retrieveOfferDetails(offerID: offerID,x: x)
            }
        }
        
        
    }

    
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
  
        return filteredloyaltyCardArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "card", for: indexPath) as! CardTableViewCell
        
        // cell.cardImage.image = UIImage(named: "AppIcon.png")
        
        cell.cardName.text = filteredloyaltyCardArray[indexPath.row].offerName
        cell.vedorName.text = filteredloyaltyCardArray[indexPath.row].vendorName
        
        let offerID = filteredloyaltyCardArray[indexPath.row].offerID
        let vendorID = filteredloyaltyCardArray[indexPath.row].vendorID
        //        let pathReference = storage.reference(withPath: "Images/\(offerID)/\(vendorID)")
        
        cell.cardImage.image = UIImage.init(named: "logo.png")
        
        
        storage.reference().child("Images/\(offerID)/\(vendorID)").downloadURL(completion: { (
            url, error) in
            if(error == nil){
                
                let urlString = url?.absoluteString
                //print(urlString)
                
                cell.cardImage.loadURL(urlString!)
                
                
            }else{
                print(error)
            }
        })
        
        
        
        
        cell.purchaseNumber.text = "Purchases : \(filteredloyaltyCardArray[indexPath.row].purchaseCount)"
        
        return cell
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "goToDetails"){
            let destination = segue.destination as! CardDetailsViewController
            //  destination.delegate = self
               print("tablePosition :",tablePosition)
            
            destination.cardId = filteredloyaltyCardArray[tablePosition].key
            destination.vendorName = filteredloyaltyCardArray[tablePosition].vendorName
            destination.offerID = filteredloyaltyCardArray[tablePosition].offerID
            destination.vendorID = filteredloyaltyCardArray[tablePosition].vendorID
            
            destination.offerName = filteredloyaltyCardArray[tablePosition].offerName
            destination.offerReward = filteredloyaltyCardArray[tablePosition].offerReward
            destination.vendorAddess = filteredloyaltyCardArray[tablePosition].vendorAddress
            destination.purchasePerReward = filteredloyaltyCardArray[tablePosition].purchasesPerReward
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tablePosition  = indexPath.row
              print("tablePositiontablePosition :",tablePosition)
        //self.performSegue(withIdentifier: "goToDetails", sender: self)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "myCard") as! CardDetailsViewController
        destination.cardId = filteredloyaltyCardArray[tablePosition].key
        destination.vendorName = filteredloyaltyCardArray[tablePosition].vendorName
        destination.offerID = filteredloyaltyCardArray[tablePosition].offerID
        destination.vendorID = filteredloyaltyCardArray[tablePosition].vendorID
        
        destination.offerName = filteredloyaltyCardArray[tablePosition].offerName
        destination.offerReward = filteredloyaltyCardArray[tablePosition].offerReward
        destination.vendorAddess = filteredloyaltyCardArray[tablePosition].vendorAddress
        destination.purchasePerReward = filteredloyaltyCardArray[tablePosition].purchasesPerReward
        
        present(destination, animated: true, completion: nil)
   
    }
    

 

}
